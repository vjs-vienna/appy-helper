import Signal from 'signals';
import TweenMax from 'gsap';
import vocab from './vocab';
import Timeline from './timeline/Timeline';
import getTimelineData from '../data/getTimelineData';

export default class AllAbout {
  constructor() {
    this.element = document.querySelector('.all-about')

    this.timeline = new Timeline();

    // console.log(this.timeline);

    this.reset();
  }

  reset(){

    this.finished = false;

    let json = vocab.getAll();

    let formattedData = getTimelineData(json);

    this.timeline.setData(formattedData);
  }

  hide(){
    this.element.style.display = "none";
  }

  show(){
    this.element.style.display = "block";
  }

  next(){

    if(!this.finished){
      this.timeline.next()
    }

  }
  toTheEnd(){

    if(!this.finished){
      this.timeline.toTheEnd()
      this.finished = true;
    }

  }
}
