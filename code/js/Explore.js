import Signal from 'signals';
import TweenMax from 'gsap';

export default class Explore {
  constructor() {
    this.element = document.querySelector('.explore');

    this.hide();
    this.onPress = new Signal();
  }

  hide() {
    this.element.style.display = 'none';
  }

  show() {
    this.element.style.display = 'block';
  }
}
