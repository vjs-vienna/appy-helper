import Signal from 'signals';
import TweenMax from 'gsap';

export default class Buttons {
  constructor() {

    this.element = document.querySelector('.reply-box');
    this.btns = Array.from(document.querySelectorAll('.reply-btn'));

    this.btns.forEach((btn) => {
      btn.addEventListener('mousedown', () => {
        TweenMax.to(btn, 0.5,
        {
          scaleX : 0.9,
          scaleY : 0.9,
        });
      });

      btn.addEventListener('mouseup', this.onUp.bind(this))
    })

    this.onPress = new Signal();
  }

  setMode(mode){

    if(mode === 'explore'){
      this.btns[0].id = 'catch-up'
      this.btns[0].textContent = "Catch me up on recent developments";

      this.btns[1].id = 'all-about'
      this.btns[1].textContent = "I want to explore the full story"

      this.btns[2].id = 'caught-up'
      this.btns[2].textContent = "I'm all caught up, take me back"
    }
    else if(mode === 'about'){
      this.btns[0].id = 'happened-next'
      this.btns[0].textContent = "What happened next?";

      this.btns[1].id = 'latest-event'
      this.btns[1].textContent = "Take me to the latest event"

      this.btns[2].id = 'caught-up'
      this.btns[2].textContent = "I'm all caught up, take me back"
    }
    // else if(mode === 'timeline-finished'){
    //   this.btns[0].id = 'happened-next'
    //   this.btns[0].textContent = "What happened next?";
    //
    //   this.btns[1].id = 'latest-event'
    //   this.btns[1].textContent = "Take me to the latest event"
    //
    //   this.btns[2].id = 'caught-up'
    //   this.btns[2].textContent = "I'm all caught up, take me back"
    // }
  }

  show(){
    this.element.style.display = "block"

    for (var i = 0; i < this.btns.length; i++) {
      let btn = this.btns[i];

      TweenMax.to(btn, 0.6,
      {
        scaleX : 1,
        scaleY : 1,
        ease : Back.easeOut,
        delay : i * 0.2
      });

      TweenMax.to(btn, 0.4,
      {
        alpha : 1,
        delay : i * 0.2
      });
    }
  }

  hideForce(){
    this.element.style.display = "none"
  }

  hide(){

    for (var i = 0; i < this.btns.length; i++) {
      let btn = this.btns[i];

      TweenMax.to(btn, 0.6,
      {
        scaleX : 0.3,
        scaleY : 0.5,
        delay : i * 0.3
      });

      TweenMax.to(btn, 0.5,
      {
        alpha : 0,
        delay : i * 0.2
      });
    }

    TweenMax.delayedCall(i * 0.4, () => {
      this.hideForce()
    })
  }

  onUp(evt){
    TweenMax.to(evt.target, 0.6,
    {
      scaleX : 1,
      scaleY : 1
    });

    this.onPress.dispatch(evt.target.id.toLowerCase());
  }
}
