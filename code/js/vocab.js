import dataJson from '../data/sheet.json';

class Vocab {
  constructor() {
    this.vocabJson = dataJson;
  }

  getKey(key) {
    if (!this.vocabJson[key]) {
      console.error('Vocab: Tried getting invalid property '+key);
      return '';
    }
    return this.vocabJson[key];
  }

  getAll(){
    return dataJson.slice();
  }
}

export default new Vocab();
