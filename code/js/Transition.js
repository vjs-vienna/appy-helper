import TweenMax from 'gsap';

class Transition{
  constructor(){
    this.purplePanel = document.querySelector('.purple-panel');
    this.whitePanel = document.querySelector('.white-panel');
    this.purplePanel.style.display = 'none';
    this.whitePanel.style.display = 'none';
  }

  show(callback){
    TweenMax.set(this.purplePanel,{xPercent : '100%',y : 0, alpha : 1});
    this.purplePanel.style.display = 'block';

    TweenMax.set(this.whitePanel,{xPercent : '100%',y : 0, alpha : 1});
    this.whitePanel.style.display = 'block';

    TweenMax.to(this.purplePanel,0.4,
    {
      xPercent : '0%',
      ease : Expo.easeOut
    });

    TweenMax.to(this.whitePanel,0.45,
    {
      xPercent : '0%',
      delay : 0.3,
      ease : Expo.easeOut,
      onComplete: function () {

        this.purplePanel.style.display = 'none';

        TweenMax.to(this.whitePanel,0.6,
        {
          alpha : 0,
          onComplete : function () {
            this.whitePanel.style.display = 'none';

            if(callback){
              callback()
            }
          },
          onCompleteScope : this
        });
      },
      onCompleteScope : this
    });
  }

  hide(callback){
    TweenMax.set(this.purplePanel,{xPercent : '0%',y : 0, alpha : 1});
    this.purplePanel.style.display = 'block';

    TweenMax.set(this.whitePanel,{xPercent : '0%',y : 0, alpha : 1});
    this.whitePanel.style.display = 'block';

    TweenMax.to(this.purplePanel,0.4,
    {
      xPercent : '100%',
      ease : Expo.easeOut
    });

    TweenMax.to(this.whitePanel,0.45,
    {
      xPercent : '100%',
      delay : 0.3,
      ease : Expo.easeOut,
      onComplete: function () {

        this.purplePanel.style.display = 'none';

        TweenMax.to(this.whitePanel,0.6,
        {
          alpha : 0,
          onComplete : function () {
            this.whitePanel.style.display = 'none';

            if(callback){
              callback()
            }
          },
          onCompleteScope : this
        });
      },
      onCompleteScope : this
    });
  }
}

const t = new Transition();
export default t;
