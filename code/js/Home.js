import Signal from 'signals';
import TweenMax from 'gsap';
import Transition from './Transition';


export default class Home {
  constructor() {
    this.element = document.querySelector('.home');
    this.btn = document.querySelector('.start-link')
    this.btn.addEventListener('click',this.hide.bind(this))

    this.onHidden = new Signal();
  }

  hideForce(){
    this.element.style.display = "none";
  }

  hide(){

    this.element.style.display = "none";

    Transition.show()
    this.onHidden.dispatch()
  }

  show(){
    this.element.style.display = "block";
  }
}
