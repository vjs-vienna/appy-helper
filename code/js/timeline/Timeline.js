import Signal from 'signals';
import TweenMax from 'gsap';
import ScrollToPlugin from "gsap/ScrollToPlugin";
import Mustache from 'mustache';
import template from 'raw-loader!./template.mustache'

export default class Timeline {
  constructor(data) {
    this.element = document.querySelector('.timeline')
    this.tempDiv = document.createElement('div');
    this.bar = document.querySelector('.timeline-bar');
    this.totalHeight = 0;
  }

  setData(data){
    // temp remove first element cause hardcoded red bubble
    data.shift()

    let output = Mustache.render(template, {elements : data});

    this.tempDiv.innerHTML = output;

    this.timelineElements = Array.from(this.tempDiv.children)

    this.timelineElementsFull = this.timelineElements.map(item => item);

    this.element.innerHTML = '';

    this.totalHeight = 0;

    TweenMax.set(this.bar, {height : 0});
  }

  next(){
    let firstTwo = this.timelineElements.splice(0,2);

    let amtToScroll = 0;

    for (var i = 0; i < firstTwo.length; i++) {
      let el = firstTwo[i]

      this.element.appendChild(el)

      TweenMax.set(el, {alpha : 0, yPercent : '10%'})

      let bbox = el.getBoundingClientRect();

      amtToScroll += bbox.height;

      TweenMax.to(el, 0.9,
      {
        yPercent : '0%',
        delay : 0.5 + i * 0.3
      });

      TweenMax.to(el, 0.4,
      {
        alpha : 1,
        delay : 0.4 + i * 0.2
      });
    }

    this.totalHeight = this.element.getBoundingClientRect().height;

    let body = document.body
    let docEl = document.documentElement
    let scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop
    scrollTop += (amtToScroll + 150)

    TweenLite.to(window, 1.9, {
      scrollTo:scrollTop,
      ease : Sine.easeOut
    });

    TweenMax.to(this.bar, 0.8, {
      height : this.totalHeight,
      ease : Expo.easeOut,
      delay : 0.3
    })

  }

  toTheEnd(){
    let elems = this.timelineElements.slice()

    let amtToScroll = 0;

    for (var i = 0; i < elems.length; i++) {
      let el = elems[i]

      this.element.appendChild(el)

      TweenMax.set(el, {alpha : 0, yPercent : '10%'})

      let bbox = el.getBoundingClientRect();

      amtToScroll += bbox.height;

      TweenMax.to(el, 0.4,
      {
        alpha : 1,
        delay : 0.96
      });
    }

    this.totalHeight = this.element.getBoundingClientRect().height;

    let body = document.body
    let docEl = document.documentElement
    let scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop
    scrollTop += (amtToScroll + 250)

    TweenLite.to(window, 0.1, {
      scrollTo:scrollTop,
      ease : Sine.easeOut
    });

    TweenMax.to(this.bar, 0.8, {
      height : this.totalHeight,
      ease : Expo.easeOut,
      delay : 0.3
    })


  }

  hide(){
    this.element.style.display = "none";
  }

  show(){
    this.element.style.display = "block";
  }
}
