import Signal from 'signals';
import TweenMax from 'gsap';
import Transition from './Transition';


export default class Header {
  constructor() {
    this.element = document.querySelector('header');
    this.title = document.querySelector('.header-text')
    this.btn = document.querySelector('.close-button')
    this.btn.addEventListener('click', () => {
      this.onPress.dispatch('caught-up')
    })

    this.onPress = new Signal();
  }

  setTitle(text){
    this.title.textContent = text;
  }

  hide(){
    this.element.style.display = "none";
  }

  show(){
    this.element.style.display = "flex";
  }
}
