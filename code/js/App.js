import Home from './Home';
import Header from './Header';
import Explore from './Explore';
import AllAbout from './AllAbout';
import Buttons from './Buttons';
import Transition from './Transition';

export default class App {
  constructor() {
    this.home = new Home();
    this.explore = new Explore();
    this.buttons = new Buttons();
    this.header = new Header();
    this.allAbout = new AllAbout();

    this.header.onPress.add(this.onButtonPressed, this);
    this.buttons.onPress.add(this.onButtonPressed, this);

    this.home.onHidden.add(this.showExplore, this);

    // TODO turn back on
    this.closeApp(true);

    // TODO turn off
    // this.home.hideForce();
    // this.showAllAbout();
    // this.buttons.show()
  }

  showExplore(){
    this.header.show();
    this.header.setTitle("Explore Story");
    this.explore.show();

    this.buttons.setMode('explore');

    TweenMax.delayedCall(0.5, () => {
      this.buttons.show();
    });
  }

  showAllAbout(){

    window.scrollTo(0, 0)
    this.explore.hide()
    this.allAbout.reset()
    this.allAbout.show()
    this.header.setTitle("What's this all about ? ")

    this.buttons.setMode('about')

    this.allAbout.next()

    Transition.show(() => {

    })
    // this.home.hide();
  }

  closeApp(noTransition){
    window.scrollTo(0, 0);
    this.home.show()
    this.allAbout.hide();
    if(!noTransition){

      Transition.show()
    }
    this.buttons.hideForce()
    this.header.hide();
    this.allAbout.reset();
  }

  onButtonPressed(id){
    console.log(id);
    if(id === 'caught-up'){
      this.closeApp();
    }
    else if (id === 'all-about') {
      this.showAllAbout()
    }
    else if(id === 'happened-next'){
      this.allAbout.next();
    }
    else if(id === 'latest-event'){
      this.allAbout.toTheEnd();
    }
  }


}
