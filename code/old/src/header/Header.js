/* eslint-disable */

import React from 'react';
import {
  Link
} from 'react-router-dom'
import './header.css';

export default class Header extends React.Component{

  componentWillReceiveProps(){

    let visible = !!(window.location.pathname !== '/');
    let visibilityClass = visible ? 'header-visible' : 'header-hidden';

    this.setState({
      visibilityClass : visibilityClass
    });
  }

  render(){

    return (
      <header className="header {this.state.visibilityClass}">
        <h2 className="header-text">The header</h2>
        <Link to="/" className="close-button">X</Link>
      </header>
    )
  }
}
