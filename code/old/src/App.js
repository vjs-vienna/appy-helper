/* eslint-disable */
import React from 'react';


import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

import Home from './home/Home';
import Header from './header/Header';
import Intro from './intro/Intro';
import Explainer from './explainer/Explainer';

export default class App extends React.Component{
  render(){
    return (
      <Router>
        <div className="wrapper">
          <Header/>
          <Route exact path="/" component={Home}/>
          <Route path="/intro" component={Intro}/>
          <Route path="/explainer" component={Explainer}/>
        </div>
      </Router>
    )
  }
}
