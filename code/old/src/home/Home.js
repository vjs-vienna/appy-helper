/* eslint-disable */

import React from 'react';
import {
  Link
} from 'react-router-dom'
import './home.css';

export default class Home extends React.Component{
  constructor(props){
    super(props);


  }

  render(){
    return (
      <div className="home">

        <img className="home__screenshot" alt="screenshot" src="img/ssh.png"/>
        <Link to="/intro" className="start-link">Start Button</Link>
  		</div>
    )
  }

}
