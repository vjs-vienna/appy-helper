var config = require("../webpack.config.js");
var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var open = require('open');

config.output.path = '/';
config.entry.app.unshift("webpack-dev-server/client?http://localhost:8080", "webpack/hot/dev-server");
var compiler = webpack(config);
var open = require("open");

console.log('what');

var server = new WebpackDevServer(compiler, {
  hot: true,
  watchOptions: {
    aggregateTimeout: 200,
    poll: true // this is the same as --watch-poll?
  },
  stats : {
    colors : true
  },
});

var port = 8080;

server.listen(port, "0.0.0.0", function(err) {
  if(err) throw new Error("webpack-dev-server", err);

  var url = 'http://localhost:'+port+'/';

  console.log('WebpackDevServer address: ', url);

  open(url);
});
