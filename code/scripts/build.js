const webpack = require('webpack');
const fse = require('fs-extra');
const path = require('path');
let config = require('../webpack.config.prod');

const OUTPUT_ROOT_DIR = path.join(process.cwd() + '/dist');
const OUTPUT_CSS_DIR = path.join(OUTPUT_ROOT_DIR + '/css');
const OUTPUT_IMG_DIR = path.join(OUTPUT_ROOT_DIR + '/img');
const compiler = webpack(config);

config.output.path = OUTPUT_ROOT_DIR;

fse
  .ensureDir(OUTPUT_ROOT_DIR)
  .then(() => {
    return fse.emptyDir(OUTPUT_ROOT_DIR);
  })
  .then(() => {
    console.log('Copying CSS files...');
    return fse.copy('css/', OUTPUT_CSS_DIR);
  })
  .then(() => {
    console.log('Copying image files...');
    return fse.copy('img/', OUTPUT_IMG_DIR);
  })
  .then(function(resolve, reject) {
    console.log('running webpack');

    return new Promise(function(resolve, reject) {
      webpack(config, function(err, stats) {
        if (err) {
          reject(err);
          return;
        }

        console.log(
          stats.toString({
            chunks: false,
            colors: true
          })
        );

        resolve();
      });
    });
  })
  .then(function() {
    console.log('Copying index...');
    return fse.copy('index.html', path.join(OUTPUT_ROOT_DIR, 'index.html'));
  })
  .then(function() {
    console.log('All done');
  })
  .catch(function(err) {
    console.error(err);
  });
