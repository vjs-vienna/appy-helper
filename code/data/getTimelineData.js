export default function getTimelineData(json) {
  return json.filter((elem) => {
    // check that is has timeline data
    return !!elem['about_option_text']
  })
  .map((elem) => {
    return {
      text : elem['about_option_text'],
      date : elem['about_option_date'],
      image : elem['temp_image'],
    }
  });
}
