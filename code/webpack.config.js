var webpack = require("webpack");
var path = require("path");

module.exports = {
  entry: {
    app: ["./js/index.js"]
  },

  output: {
    path: "/",
    filename: "js/build.js"
  },
  devServer: {
    host: "0.0.0.0"
  },

  // correct
  module: {
    rules: [
    {
      test: /\.js$/,
      exclude: [
        /node_modules/,
      ],
      loader: 'babel-loader'
    },
    {
      test: /\.html|.css$/,
      loader: "raw-loader"
    }
  ]
  },
  resolve: {
    modules: [
      path.join(__dirname, "js"),
      "node_modules"
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ]
};
