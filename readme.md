## BBC Visual Journalism, Editor's Lab Final 2017 ##

A conversational interface that will sit within long-running complex stories that can help users catch up with recent developments or learn all about the background to this story in an accessible way, tailored to their needs.
